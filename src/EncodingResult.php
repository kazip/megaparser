<?php
/**
 * Created by PhpStorm.
 * User: Hirnhamster
 * Date: 12.10.2014
 * Time: 21:46
 */
namespace parserbot\megaparser;
class EncodingResult{
    /**
     * Encoding before the conversion.
     * @var string
     */
    private string $originalEncoding;
    /**
     * Encoding after the conversion.
     * @var string
     */
    private string $targetEnconding;
    /**
     * Content after the conversion (caution: the content itself remained untouched, e.g. no <meta> tags have been changed)
     * @var string
     */
    private string $targetContent;
    /**
     * Headers after the conversion ('content-type' is updated)
     * @var array
     */
    private array $targetHeaders;
    /**
     * @param string $originalEncoding
     * @param string $targetEnconding
     * @param array $targetHeaders
     * @param string $targetContent
     */
    function __construct(string $originalEncoding, string $targetEnconding, array $targetHeaders, string $targetContent)
    {
        $this->originalEncoding = $originalEncoding;
        $this->targetEnconding = $targetEnconding;
        $this->targetHeaders = $targetHeaders;
        $this->targetContent = $targetContent;
    }
    /**
     * @return string
     */
    public function getOriginalEncoding(): string
    {
        return $this->originalEncoding;
    }
    /**
     * @param string $originalEncoding
     */
    public function setOriginalEncoding(string $originalEncoding): void
    {
        $this->originalEncoding = $originalEncoding;
    }
    /**
     * @return string
     */
    public function getTargetContent(): string
    {
        return $this->targetContent;
    }
    /**
     * @param string $targetContent
     */
    public function setTargetContent(string $targetContent): void
    {
        $this->targetContent = $targetContent;
    }
    /**
     * @return string
     */
    public function getTargetEnconding(): string
    {
        return $this->targetEnconding;
    }
    /**
     * @param string $targetEnconding
     */
    public function setTargetEnconding(string $targetEnconding): void
    {
        $this->targetEnconding = $targetEnconding;
    }
    /**
     * @return array
     */
    public function getTargetHeaders(): array
    {
        return $this->targetHeaders;
    }
    /**
     * @param array $targetHeaders
     */
    public function setTargetHeaders(array $targetHeaders): void
    {
        $this->targetHeaders = $targetHeaders;
    }
}