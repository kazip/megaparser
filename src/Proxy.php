<?php

namespace parserbot\megaparser;
class Proxy extends ActiveRecord{
    public string $proxy = '';

    public static function getNext() {
        return new Proxy();
    }
}