<?php

namespace parserbot\megaparser;

use Psr\Http\Message\RequestInterface;

class EffectiveUrlMiddleware
{
    /**
     * @var RequestInterface
     */
    private RequestInterface $lastRequest;

    /**
     * @param RequestInterface $request
     *
     * @return RequestInterface
     */
    public function __invoke(RequestInterface $request): RequestInterface
    {
        $this->lastRequest = $request;
        return $request;
    }

    /**
     * @return RequestInterface
     */
    public function getLastRequest(): RequestInterface
    {
        return $this->lastRequest;
    }
}