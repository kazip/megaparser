<?php

namespace parserbot\megaparser;

interface ProxyInterface
{
    public function getProxy(): string;
}