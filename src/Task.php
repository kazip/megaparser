<?php
namespace parserbot\megaparser;

use Psr\Http\Message\ResponseInterface;

class Task {

    public string $method;
    public array $headers;
    public string $proxy;
    public string $cookie;
    public string $url;
    public bool $completed;
    public bool $finished;
    public bool $failed;
    public int $retry;
    public string $callback;
    public array $params;
    public string $html;
    public array $custom;
    public int $concurrency;
    public array $multipart;
    public bool $preserve_proxy;
    public array $curl_options;
    public bool $verify;
    public ?ResponseInterface $response = null;
    public string $effective_url = '';
    public string $base_url = '';
    public array $options = [];
    public string $failback;
    public int $priority;
    public bool $http_errors_on = true;

    const GET = "GET";
    const POST = "POST";
    const PUT = "PUT";
    const PATCH = "PATCH";
    const DELETE = "DELETE";
    /**
     * @var array
     */
    private array $json;

    public function __construct($url, $method = self::GET, $options = [])
    {
        $this->method = $method;
        $this->options = $options;
        $this->retry = $options['retry'] ?? 0;
        $this->completed = $options['completed'] ?? false;
        $this->finished = $options['finished'] ?? false;
        $this->failed = $options['failed'] ?? false;
        $this->curl_options = $options['curl_options'] ?? [];
        $this->verify = $options['verify'] ?? true;

        $this->preserve_proxy = $options['preserve_proxy'] ?? false;

        if (isset($options['cookie'])) {
            $this->cookie = $options['cookie'];
        } elseif (isset($options['cookies'])) {
            $this->cookie = $options['cookies'];
        } else {
            $this->cookie = '';
        }

        $this->proxy = $options['proxy'] ?? '';
        $this->headers = $options['headers'] ?? [];
        $this->callback = $options['callback'] ?? false;
        $this->failback = $options['failback'] ?? false;
        $this->params = $options['params'] ?? [];
        $this->concurrency = $options['concurrency'] ?? false;
        $this->custom = $options['custom'] ?? [];
        $this->multipart = $options['multipart'] ?? [];
        $this->json = $options['json'] ?? [];
        $this->http_errors_on = $options['http_errors'] ?? true;
        $this->url = $url;


        # Приоритет - высший -бесконечность, низший плюс бесконечность. По умолчанию 0
        $this->priority = (int)($options['priority'] ?? 0);

        $this->base_url = $options['base_url'] ?? '';
    }

    public function getPriority(): int
    {
        return $this->priority;
    }

    public function setPriority($priority): void
    {
        $this->priority = $priority;
    }

    public function getCopy(): Task
    {
        return new Task($this->url, $this->method, $this->options);
    }

    public function getHttpErrorsOn()
    {
        return $this->http_errors_on;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function setBaseUrl($url): void
    {
        $this->base_url = $url;
    }

    public function getBaseUrl(): string
    {
        return $this->base_url;
    }

    public function setEffectiveUrl($url): void
    {
        $this->effective_url = $url;
    }

    public function getEffectiveUrl(): string
    {
        return $this->effective_url?:$this->url;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    public function setResponse(ResponseInterface $response): void
    {
        $this->response = $response;
    }

    public function getVerify(): bool
    {
        return $this->verify;
    }

    public function getConcurrency(): int
    {
        return $this->concurrency;
    }

    public function getCallback(): string
    {
        return $this->callback;
    }

    public function setCallback(string $callback): void
    {
        $this->callback = $callback;
    }

    public function getFailback(): string
    {
        return $this->failback;
    }

    public function setFailback(string $failback): void
    {
        $this->failback = $failback;
    }

    public function setHtml(string $html): void
    {
        $this->html = $html;
    }

    public function getHtml(): string
    {
        return $this->html;
    }

    public function setCustom(string $name, mixed $value): void
    {
        $this->custom[$name] = $value;
    }

    public function getCustom(string $name, string $default=''): mixed
    {
        return $this->custom[$name] ?? $default;
    }

    public function getCustomAll(): array
    {
        return $this->custom;
    }

    public function getUrl(): string
    {
        return $this->url;
    }

    public function setFinished(bool $finished): void
    {
        $this->finished = $finished;
    }

    public function isPreserveProxy(): bool
    {
        return $this->preserve_proxy;
    }

    public function isFinished(): bool
    {
        return $this->finished;
    }

    public function isCompleted(): bool
    {
        return $this->completed;
    }

    public function getProxy(): string
    {
        return $this->proxy;
    }

    public function setProxy(string $proxy): void
    {
        $this->proxy = $proxy;
    }

    public function getCookies(): string
    {
        return $this->cookie;
    }

    public function setCookies(string $cookie): void
    {
        $this->cookie = $cookie;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }

    public function setHeaders(array $headers): void
    {
        $this->headers = $headers;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getRetry(): int
    {
        return $this->retry;
    }

    public function incRetry(): void
    {
        $this->retry = $this->retry + 1;
    }

    public function complete(): void
    {
        $this->completed = true;
    }

    public function finish(): void
    {
        $this->finished = true;
    }

    public function fail(): void
    {
        $this->completed = true;
        $this->failed = true;
    }

    public function getFailed(): bool
    {
        return $this->failed;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function getCurlOptions(): array
    {
        return $this->curl_options;
    }

    public function getMultipart(): array
    {
        return $this->multipart;
    }

    public function getJson(): array
    {
        return $this->json;
    }

    public function getProxyString(): string
    {
        $proxy = $this->getProxy();
        if (is_object($proxy)) {
            $proxy = $proxy->proxy;
        }

        return $proxy;
    }

    function __destruct()
    {
        $this->destroy();
    }

    public function destroy(): void
    {
        foreach ($this as $k=>$v) {
            unset($this->{$k});
        }
    }

}