<?php

namespace parserbot\megaparser;

class Query {

    protected string $class;

    public function __construct($class)
    {
        $this->class = $class;
    }

    public function all(): array
    {
        return [new $this->class()];
    }

    public function one()
    {
        return new $this->class();
    }

    public function where($condition): Query
    {
        return new Query($this->class);
    }

    public function orderBy($condition): Query
    {
        return new Query($this->class);
    }
}