<?php

namespace parserbot\megaparser;

interface ProxerInterface {

    function blockProxy(ProxyInterface $proxy);
    function getProxy(): ?ProxyInterface;
    function delayProxy(ProxyInterface $proxy): bool;
    function blockAndChangeProxy();
    function changeProxy(bool $block = false);
    function proxyRenew(string $filename): bool;
    public function setProxy(): bool;
    function __construct(array $options);

}