<?php

namespace parserbot\megaparser\Test;

use parserbot\megaparser\ProxyInterface;

class TestProxer implements \parserbot\megaparser\ProxerInterface
{

    function setProxy(): bool
    {
        // TODO: Implement setProxy() method.
        return true;
    }

    function blockProxy(ProxyInterface $proxy)
    {
        // TODO: Implement blockProxy() method.
    }

    function getProxy(): ?ProxyInterface
    {
        // TODO: Implement getProxy() method.
        return null;
    }

    function delayProxy(ProxyInterface $proxy): bool
    {
        // TODO: Implement delayProxy() method.
        return true;
    }

    function blockAndChangeProxy()
    {
        // TODO: Implement blockAndChangeProxy() method.
    }

    function changeProxy(bool $block = false)
    {
        // TODO: Implement changeProxy() method.
    }

    function proxyRenew(string $filename): bool
    {
        // TODO: Implement proxyRenew() method.
        return true;
    }

    public function __construct(array $options=[])
    {
    }
}