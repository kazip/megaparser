<?php

namespace parserbot\megaparser\Test;

use parserbot\megaparser\MegaParser;
use parserbot\megaparser\MessengerInterface;
use parserbot\megaparser\ProxerInterface;
use parserbot\megaparser\ProxyInterface;
use parserbot\megaparser\Task;


class Test extends MegaParser
{

    public function __construct(ProxerInterface $proxer, MessengerInterface $messenger, $options = [])
    {
        if (!isset($options['messenger'])) {
            $options['messenger'] = 'parserbot\\parser\\tests\\Messenger';
        }

        parent::__construct($proxer, $messenger, $options);
        $this->config['base_url'] = 'https://maps.yandex.ru';
        if (isset($options['concurrency'])) {
            $this->concurrency = $options['concurrency'];
        } else {
            $this->concurrency = 5;
        }

    }

    public function taskGenerator(): ?\Generator
    {
        yield new Task("https://a-api.fix-price.com/buyer/v1/location/city", Task::GET, ['callback'=>'Geo', 'custom'=>['skip'=>true],
            'headers'=>[
                'User-Agent' => 'BUYER-FRONT-ANDROID 3.7'
            ]
        ]);
    }

    public function processGeo(Task $task): \Generator
    {

        $task->complete();

        $json = json_decode($task->getHtml());
        $cityId = $json[0]->id;
        yield new Task("https://a-api.fix-price.com/buyer/v2/category/2004", Task::GET, ['callback'=>'Category', 'headers'=>['x-city'=>$cityId,  'User-Agent' => 'BUYER-FRONT-ANDROID 3.7'], 'custom'=>['cityId'=>$cityId]]);
    }

    public function processCategory(Task $task): ?\Generator
    {
        $task->complete();
        $data = json_decode($task->getHtml());
        $cityId = $task->getCustom('cityId');
        foreach ($data->subcatalogs as $category) {
            $categoryId = $category->id;
            yield new Task("https://a-api.fix-price.com/buyer/v2/product/filter", Task::POST, ['callback'=>'Items', 'json'=>['category'=>[$categoryId]], 'headers'=>['x-city'=>$cityId,  'User-Agent' => 'BUYER-FRONT-ANDROID 3.7'], 'custom'=>['cityId'=>$cityId]]);
        }
    }

    public function processItems(Task $task)
    {
        $task->complete();
        $data = json_decode($task->getHtml());
        print_r($data);
    }

}