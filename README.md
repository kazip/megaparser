# Фреймворк для парсинга #

Данный фреймворк позволяет быстро создавать всевозможные парсеры.

### Как работает фреймворк? ###

* Задания (Task)
* Класс парсера (MegaParser)
* Функции процессинга (process...)


Ниже приведен пример модуля для парсинга:

```
#!php

<?php

use kazip\megaparser\MegaParser;
use kazip\megaparser\Task;

class SiteParse extends MegaParser
{
	
	private $export;

	function __construct($options)
	{
		parent::__construct($options);
		$this->config['base_url'] = 'http://site.com';
		$this->export = new Export();
	}

	function taskGenerator()
	{
		yield new Task("http://site.com/start", Task::GET, ["callback"=>"Page"]);
	}

	function processPage(Task $task)
	{
		$dom = \phpQuery::newDocumentHTML($task->getHtml());
		
		$items = $dom->find('div.item a');
		foreach ($items as $item) {
			$url = pq($item)->attr('href');
			yield new Task($url, Task::GET, ["callback"=>"Item"]);
		}

		\phpQuery::unloadDocument($dom);
	}

	function processItem(Task $task)
	{
		$dom = \phpQuery::newDocumentHTML($task->getHtml());

		$title = $dom->find('h1');
		$item = [];
		$item['title'] = $title;
		$item['url'] = $task->getUrl();

		$this->export->exportItem($item);

		\phpQuery::unloadDocument($dom);
	}

}

?>
```

##Как работает фреймворк##

Класс модуля парсинга наследуется от **MegaParser**
Нужно обязательно в конструкторе вызвать родительский конструктор для инициализации.

Парсинг начинается с задач, которые выдаются генератором задач - метод **taskGenerator()**,
метод должен генерировать объекты **Task**, в которых указывается *url*, метод запроса и также функция обработки результата - *['callback'=> 'Result']*

По окончании выполнения запроса будет вызван метод processResult - где '*Result*' - это название функции обработки, указанное в параметре *callback*.

В метод processResult передаётся объект **Task**.
У даного объекта доступны несколько методов:


* **getHtml()** - получить тело ответа сервера (не обязательно это будет html)
* **getUrl()** - получить url запроса
* **getCustom(param, default)** -  получить пользовательский параметр переданный в параметре ['custom']
* **getCustomAll()** - получить массив пользовательских параметров
* **getEffectiveUrl()** - получить эффективный url - то есть куда в итоге был сделан редирект после запроса
* **getProxy()** - получить объект Proxy - который был передан в параметре ['proxy']
* **getCookies()** - получить объект Cookie, который был передан в параметре ['cookie']
* **getMethod()** - получить метод запроса (GET, POST) 
* и некоторые другие.

Парсинг поддерживает прокси и куки - объекты Proxy и Cookie (это объект например CookieJar) передаются при создании задания - 

*yield new Task(url, method, ['cookie'=>new CookieJar(), 'proxy'=>new Proxy(), 'callback'=>callback])*;

Объект Proxy - умеет сам менять прокси если не удалось соединиться через текущий прокси.

Также есть параметры у Task - '*preserve_proxy*' - это значит не менять прокси даже при ошибке.

У класса модуля парсинга есть переменная concurrency - которая означает число потоков для одновременного скачивания. То есть сколько одновременно может выполняться задач. Реализовано в итоге через асинхронные запросы Guzzle. Выполнение функций обработки происходит последовательно. То есть здесь нет никакой многозадачности.

Модуль парсинга может и должен использовать метод **$this->addMessage(message)** для вывода информации в лог.

Если модуль экспортирует что-то - то можно использовать внешний объект **$this->export = new Export()** с методами **exportItem(item), exportCategory(category),** или другими по согласованию.